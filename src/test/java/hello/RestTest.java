package hello;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestTest {

    @Autowired
    TestRestTemplate restTemplate;

    JacksonTester<Greeting> json;

    @Before
    public void setup() {
        ObjectMapper objectMapper = new ObjectMapper();
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void testDefault() throws IOException {

        ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/greeting", String.class);

        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.OK);

        String body = responseEntity.getBody();
        Greeting greeting = json.parseObject(body);
        assertThat(greeting.getContent()).contains(GreetingController.DEFAULT_NAME);
    }

    @Test
    public void testCustomName() throws IOException {


        String customName = "Bob";
        ResponseEntity<String> responseEntity = this.restTemplate.getForEntity("/greeting?name="+customName, String.class);

        HttpStatus statusCode = responseEntity.getStatusCode();
        assertThat(statusCode).isEqualTo(HttpStatus.OK);

        String body = responseEntity.getBody();
        Greeting greeting = json.parseObject(body);
        assertThat(greeting.getContent()).contains(customName);
    }


}