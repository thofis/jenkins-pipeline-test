package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Rest-Endpoint for greetings.
 */
@RestController
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    final static String DEFAULT_NAME = "World";

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue=DEFAULT_NAME) String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}