node() {

    stage("Checkout") {
        checkout scm
    }

    stage ("Build") {
        withMaven() {
            // Run the maven build
            sh "mvn clean package source:jar javadoc:javadoc -DskipTests=true"

            step([$class: 'ArtifactArchiver', artifacts: 'target/*.jar'])
            step([$class: 'JavadocArchiver', javadocDir: 'target/site/apidocs/'])
        }
    }

    stage ("Code Analysis") {
        withMaven() {
            // run tests
            sh "mvn checkstyle:checkstyle pmd:pmd findbugs:findbugs"

            // Use fully qualified hudson.plugins.checkstyle.CheckStylePublisher if JSLint Publisher Plugin or JSHint Publisher Plugin is installed
            step([$class: 'hudson.plugins.checkstyle.CheckStylePublisher', pattern: '**/target/checkstyle-result.xml'])
            step([$class: 'PmdPublisher', pattern: '**/target/pmd.xml'])
            step([$class: 'FindBugsPublisher', pattern: '**/findbugsXml.xml'])
            step([$class: 'AnalysisPublisher'])
        }

    }

    stage ("SonarQube") {

        //def sonarqubeScannerHome = tool name: 'SonarQubeScanner', type: 'hudson.plugins.sonar.SonarRunnerInstallation'
        sh "/home/tom/tools/sonar-runner-2.4/bin/sonar-runner -e -Dsonar.host.url=http://localhost:9000"
    }

    stage ("Test") {
        withMaven() {
            // run tests
            sh "mvn test"

            // publish JUnit results to Jenkins
            step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/*.xml'])
        }
    }

    stage ("Dockerize") {
        // add jenkins user to docker group in order to make this work
        // sudo gpasswd -a jenkins docker
        //sh "docker build -t jenkins-pipeline-test ."

        docker.build('jenkins-pipeline-test')
    }
}